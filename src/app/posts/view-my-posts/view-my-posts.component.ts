import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import * as firebase from 'firebase';
import { Posts } from 'src/app/shared/services/post';
import { PostsService } from 'src/app/shared/services/posts.service';
import { User } from 'src/app/shared/services/user';

@Component({
  selector: 'app-view-my-posts',
  templateUrl: './view-my-posts.component.html',
  styleUrls: ['./view-my-posts.component.css']
})
export class ViewMyPostsComponent implements OnInit {
  
  posts!: Posts[];

  constructor( private PostsService: PostsService, 
    private fireStorage: AngularFireStorage, 
    private firestore: AngularFirestore,
    public afAuth: AngularFireAuth ) { }

  ngOnInit(): void {
    this.afAuth.currentUser.then(res=>{
      if(res) {
        const userId = res.uid;
        this.PostsService.getMyPosts(userId).valueChanges().subscribe(data => {
          this.posts = data.map(doc => {
            const postDetails = {
              id: doc.id,
              title: doc.title,
              description: doc.description,
              creationDate: doc.creationDate,
              imagePath: doc.imagePath,
              userId: doc.userId
            } as Posts
          this.firestore.collection('users').doc(postDetails.userId)
              .ref.get()
              .then(doc => {
                const data = doc.data() as User
                postDetails.userId = data.name
              })
            
            
          postDetails.creationDate = postDetails.creationDate.toDate()
          return postDetails
        })
      })
      }
    })
  }

}
