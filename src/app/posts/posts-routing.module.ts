import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ViewAllPostsComponent } from './view-all-posts/view-all-posts.component';
import { AddNewPostsComponent } from './add-new-posts/add-new-posts.component';
import { ViewMyPostsComponent } from './view-my-posts/view-my-posts.component';

const routes: Routes = [
  {
    path: '',
    component: ViewAllPostsComponent
  },
  {
    path: 'add',
    component: AddNewPostsComponent
  },
  {
    path: 'myblog',
    component: ViewMyPostsComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }
