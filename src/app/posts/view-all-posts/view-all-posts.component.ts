import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import * as firebase from 'firebase';
import { Posts } from 'src/app/shared/services/post';
import { PostsService } from 'src/app/shared/services/posts.service';
import { User } from 'src/app/shared/services/user';

@Component({
  selector: 'app-view-all-posts',
  templateUrl: './view-all-posts.component.html',
  styleUrls: ['./view-all-posts.component.css']
})
export class ViewAllPostsComponent implements OnInit {

  posts!: Posts[];

  constructor( private PostsService: PostsService, private fireStorage: AngularFireStorage, private firestore: AngularFirestore ) { }

  ngOnInit(): void {
    this.PostsService.getPosts().subscribe(data => {
      this.posts = data.map(e => {
        const postDetails = {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as {}
        } as Posts
        this.firestore.collection('users').doc(postDetails.userId)
          .ref.get()
          .then(doc => {
            const data = doc.data() as User
            postDetails.userId = data.name
          })
        
        
        postDetails.creationDate = postDetails.creationDate.toDate()
        return postDetails
      })
    })
  }
}
