import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsRoutingModule } from './posts-routing.module';
import { MaterialModule } from '../material/material.module';

import { ViewAllPostsComponent } from './view-all-posts/view-all-posts.component';
import { AddNewPostsComponent } from './add-new-posts/add-new-posts.component';

import { AngularFireStorageModule } from '@angular/fire/storage'
import { AngularFireModule } from '@angular/fire'
import { FormsModule } from '@angular/forms';
import { ViewMyPostsComponent } from './view-my-posts/view-my-posts.component';

@NgModule({
  declarations: [
    ViewAllPostsComponent,
    AddNewPostsComponent,
    ViewMyPostsComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    PostsRoutingModule,
    AngularFireStorageModule,
    AngularFireModule,
    FormsModule
  ],
  exports: [
    ViewAllPostsComponent,
    AddNewPostsComponent,
    ViewMyPostsComponent
  ]
})
export class PostsModule { }
