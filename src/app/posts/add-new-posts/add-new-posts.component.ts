import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { PostsService } from 'src/app/shared/services/posts.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { Posts } from 'src/app/shared/services/post';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-new-posts',
  templateUrl: './add-new-posts.component.html',
  styleUrls: ['./add-new-posts.component.css']
})
export class AddNewPostsComponent implements OnInit {

  path: any;
  title: string = "";
  description: string = "";
  posts: Posts = new Posts;
  downloadURL!: Observable<string>;
  

  constructor(public afAuth: AngularFireAuth,
    private afStorage: AngularFireStorage, 
    private PostsService: PostsService,
    private router: Router ) { }


  ngOnInit(): void {
  }

  uploadImage(imageEvent: Event) {
    this.path  = (imageEvent.target as HTMLInputElement)?.files?.[0]

  }

  addBlog(event: Event) {
    event.preventDefault()
    this.afAuth.currentUser.then(res => {
      if(res) {
        this.posts.userId = res.uid;
        this.posts.creationDate = new Date();
        if(this.path){
          const filePath = `blog-images/${Math.random()}-${this.path.name}`;
          const fileRef = this.afStorage.ref(filePath);
          let task = this.afStorage.upload(filePath, this.path)
          task.snapshotChanges().pipe(
            finalize(()=>{
              fileRef.getDownloadURL().subscribe(url => {
                this.posts.imagePath = url
                this.posts.title = this.title
                this.posts.description = this.description
        
                this.PostsService.createPost(this.posts);
                
                console.log("Successfully uploaded")
                this.router.navigate(['/posts'])

              });
            })
          ).subscribe()
        }

      }

    }).catch(err=>{
      console.log("error: ", err)
    });
  }
}
