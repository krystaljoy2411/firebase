import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewPostsComponent } from './add-new-posts.component';

describe('AddNewPostsComponent', () => {
  let component: AddNewPostsComponent;
  let fixture: ComponentFixture<AddNewPostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddNewPostsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
