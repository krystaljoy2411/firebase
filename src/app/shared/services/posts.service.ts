import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import firebase from "firebase/app"
import { Posts } from './post';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private firestore: AngularFirestore) { }

  getMyPosts(userId:string) {
    return this.firestore.collection<Posts>('posts', ref => ref.where('userId', '==', userId))
  }

  getPosts() {
    return this.firestore.collection('posts').snapshotChanges();
  }

  createPost(post: Posts){
    return this.firestore.collection('posts').add(Object.assign({},post));
  }

  updatePolicy(postId: string, post: Posts){
    this.firestore.doc('posts/' + postId).update(post);
  }

  deletePolicy(postId: string){
    this.firestore.doc('posts/' + postId).delete();
  }
}
