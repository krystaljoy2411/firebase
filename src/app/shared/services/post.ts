export class Posts {
    id?: string;
    title!: string;
    creationDate!: any;
    description!: string;
    imagePath!: string;
    userId!: any;
}