import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import firebase from "firebase/app"
import { User } from './user'
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})

export class AuthService {

  user: User = new User;
  

  constructor(private angularFireAuth: AngularFireAuth, 
    private afStorage: AngularFireStorage,
    private firestore: AngularFirestore,
    private router: Router) { }

  checkAuth(){
    return this.angularFireAuth;
  }

  GoogleLogin() {
    this.angularFireAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(res => {
      if(res.user){
        this.user.uid = res.user.uid
        this.user.email = res.user.email!
        this.user.name = res.user.displayName!

        this.firestore.collection('users').doc(res.user.uid).set(Object.assign({},this.user));
        console.log('Login Successful')
        this.router.navigate(['/posts'])
      }
    })
  }

  SignUp(email: string, password: string, name: string) {
    this.angularFireAuth.createUserWithEmailAndPassword(email, password).then(res => {
      res.user?.updateProfile({
        displayName: name,
      }).then(result => {
        if(res.user){
          this.user.uid = res.user.uid
          this.user.email = res.user.email!
          this.user.name = res.user.displayName!
  
          this.firestore.collection('users').doc(res.user.uid).set(Object.assign({},this.user));
        }
      }

      )
      console.log('Successfully signed up', res)
      this.router.navigate(['/posts'])
    }).catch(error => {
      console.log('something went wrong', error.message);
    });
  }

  SignIn(email: string, password: string) {
    this.angularFireAuth.signInWithEmailAndPassword(email, password).then(res => {
      console.log(`You're in!`);
      this.router.navigate(['/posts'])
    }).catch(err => {
      console.log('Something went wrong:',err.message);
    });
  }
    
  SignOut() {
    this.angularFireAuth.signOut();
  }
}
