import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'user', loadChildren: () => import('./user/user-routing.module').then(mod => mod.UserRoutingModule) },
  { path: 'posts', loadChildren: () => import('./posts/posts-routing.module').then(mod => mod.PostsRoutingModule) }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
