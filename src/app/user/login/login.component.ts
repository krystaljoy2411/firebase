import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public email = "soumyaedappilly@gmail.com"
  public password = "24111999"

  constructor(public auth: AuthService) { }

  LoginUsingGoogle() {
    this.auth.GoogleLogin()
    
  }

  LogInUsingEmail(event: Event) {
    event.preventDefault()
    this.auth.SignIn(this.email, this.password)
  }

  ngOnInit(): void {
  }

}
