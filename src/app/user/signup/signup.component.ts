import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  name:string = "";
  email:string = ""
  password:string = ""

  constructor(public auth: AuthService) { }

  SignUpWithEmail(event: Event) {
    event.preventDefault()
    this.auth.SignUp(this.email, this.password, this.name)
  }

  ngOnInit(): void {
  }

}
